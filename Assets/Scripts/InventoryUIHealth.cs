using UnityEngine;
using TMPro;

public class InventoryUIHealth : MonoBehaviour
{
    private TextMeshProUGUI objectText;

    // Start is called before the first frame update
    void Start()
    {
        objectText = GetComponent<TextMeshProUGUI>();
    }

    public void UpdateHealthText(PlayerInventory playerInventory)
    {
        objectText.text = playerInventory.NumberOfHealth.ToString()+"/2";
    }
}

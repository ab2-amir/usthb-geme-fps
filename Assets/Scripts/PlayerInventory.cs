using UnityEngine;
using UnityEngine.Events;

public class PlayerInventory : MonoBehaviour
{
    public int NumberOfAmmo { get; private set; }
    public int NumberOfWeapon { get; private set; }
    public int NumberOfHealth { get; private set; }

    public UnityEvent<PlayerInventory> OnObjectCollected;

    //student id card
    public void AmmoCollected()
    {
        NumberOfAmmo++;
        OnObjectCollected.Invoke(this);
    }

    //relevé de note
    public void WeaponCollected()
    {
        NumberOfWeapon++;
        OnObjectCollected.Invoke(this);
    }

    public void HealthCollected()
    {
        NumberOfHealth++;
        OnObjectCollected.Invoke(this);
    }
}

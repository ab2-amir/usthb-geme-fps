using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class WinAndLoseMenu : MonoBehaviour
{
    // Start is called before the first frame update    public void LoadMenu()
    public void LoadMenu()
    {
        Debug.Log("LoadMenu..");
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }
    // private void OnEnable()
    // {
    //     controls.Enable();
    // }

    // private void OnDisable()
    // {
    //     controls.Disable();
    // }
}

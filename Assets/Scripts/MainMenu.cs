using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject lvl2;
    public string level1win;

    void Start()
    {
        // UnityEngine.Debug.Log("active"+ CountDownLogic.WinLevel1);
        level1win = PlayerPrefs.GetString("Win", "false").ToString();
        
        if(level1win == "true")
        {
            lvl2.SetActive(true);
        }  
    }

    public void PlayOpenWorld ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    
    public void PlayLevel1 ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }

    public void PlayLevel2 ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 3);
    }

    public void QuitGame()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }

}

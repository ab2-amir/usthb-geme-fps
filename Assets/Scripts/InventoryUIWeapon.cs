using UnityEngine;
using TMPro;

public class InventoryUIWeapon : MonoBehaviour
{
    private TextMeshProUGUI objectText;

    // Start is called before the first frame update
    void Start()
    {
        objectText = GetComponent<TextMeshProUGUI>();
    }

    public void UpdateWeaponText(PlayerInventory playerInventory)
    {
        objectText.text = playerInventory.NumberOfWeapon.ToString()+"/1";
    }
}

using UnityEngine;
using TMPro;

public class InventoryUIAmmo : MonoBehaviour
{
    private TextMeshProUGUI objectText;

    // Start is called before the first frame update
    void Start()
    {
        objectText = GetComponent<TextMeshProUGUI>();
    }

    public void UpdateAmmoText(PlayerInventory playerInventory)
    {
        objectText.text = playerInventory.NumberOfAmmo.ToString()+"/2";
    }
}
